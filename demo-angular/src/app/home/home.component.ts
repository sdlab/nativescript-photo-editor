import { Component, OnInit, NgZone } from "@angular/core";
import { PhotoEditor } from "nativescript-photo-editor";
import { ImageSource, fromFileOrResource } from 'tns-core-modules/image-source/image-source';
import { Router } from "@angular/router";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    
    constructor(private zone:NgZone, private router: Router) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    changePage() {
        this.zone.runOutsideAngular(() => {
            this.router.navigate(['home/home2']);
        });
    }

    editImage() {

        this.zone.runOutsideAngular(() => {

            /*
            var intervalTime = (new Date()).getTime();
            setInterval(() => {
                console.log('setInterval 1000 after ' + ((new Date()).getTime() - intervalTime) + 'ms');
                intervalTime = (new Date()).getTime();
            }, 1000);
            */

            //console.log('zone stable AAA ', this.zone.isStable);
            console.log('*********** EDIT IMAGE *********');

            this.zone.onStable.subscribe(() => console.log('ZONE STABLE !!'));
            this.zone.onUnstable.subscribe(() => console.log('ZONE UNSTABLE  !!'));

            this.zone.onMicrotaskEmpty.subscribe(() => console.log('ZONE MICROTASKEMPTY !!'));

            const photoEditor = new PhotoEditor();
            const imageSource: ImageSource = fromFileOrResource("~/app/test-image.jpg");
        
            //console.log('zone stable BBB ', this.zone.isStable);

            photoEditor.editPhoto({
                imageSource: imageSource, // originalImage.imageSource,
                hiddenControls: [],
            });


            /*
            photoEditor.editPhoto({
                imageSource: imageSource, // originalImage.imageSource,
                hiddenControls: [],
            }).then((newImage: ImageSource) => {
                console.log('NEW IMAGE: ', newImage.height, newImage.width);
            }).catch((e) => {
                console.log('****** ERROR.', e);

                //this.zone.

                /*
                const photoEditor2 = new PhotoEditor();
                photoEditor2.editPhoto({
                    imageSource: imageSource, // originalImage.imageSource,
                    hiddenControls: [],
                }).then((newImage: ImageSource) => {
                    console.log('NEW IMAGE: ', newImage.height, newImage.width);
                }).catch((e) => {
                    console.log('***** ERROR2 !!', e);

                    var time = (new Date()).getTime();
                    setTimeout(() => {
                        console.log('setTimeout 1000 after ' + ((new Date()).getTime() - time) + 'ms');
                    }, 1000);

                });
                */

                //this.router.navigate(['home/home2']);

                /*
                var time = (new Date()).getTime();
                setTimeout(() => {
                    console.log('setTimeout 1000 after ' + ((new Date()).getTime() - time) + 'ms');
                }, 1000);
                //setTimeout(() => console.log('setTimeout 100 after ' + ((new Date()).getTime() - time) + 'ms'), 100);
                */
            //});
            

        });
    }
}

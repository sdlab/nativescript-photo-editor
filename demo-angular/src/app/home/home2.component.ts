import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: "Home2",
    moduleId: module.id,
    templateUrl: "./home2.component.html"
})
export class Home2Component{
    
    constructor(private router: Router) {
    }

    ngOnInit() {
        //this.router.navigate(['home']);
    }

    changePage() {
        this.router.navigate(['home']);
    }

}

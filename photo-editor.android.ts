import * as application from "application";
import { ImageSource } from "image-source";

import { EditPhotoOptions, PhotoEditor as PhotoEditorBase } from ".";

declare const com: any;

export class PhotoEditor implements PhotoEditorBase {
    private static readonly EDIT_PHOTO_REQUEST = 9090;

    public editPhoto(options: EditPhotoOptions) {
        options.hiddenControls = options.hiddenControls || [];

        return new Promise<ImageSource>((resolve, reject) => {         
            const intent = new android.content.Intent(application.android.foregroundActivity, com.tangrainc.photoeditor.PhotoEditorActivity.class);
            application.android.foregroundActivity.startActivityForResult(intent, PhotoEditor.EDIT_PHOTO_REQUEST);    
        });
    }
}